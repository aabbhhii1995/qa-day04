import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {

	//test case 1 : Creating a new cart has 0 items
	@Test
	public void testCreateNewCart() {
		//1. creating a shopping cart
		
		ShoppingCart cart = new ShoppingCart();
		
		
		
		//2. check how many items are in it
		
		int numItems = cart.getItemCount();
		
		
		
		//3. compare your expected with actual
		
		assertEquals(0,numItems);
	}
	
	
	
	
	//test case 2 : Empty cart has 0 items
	
		@Test
		public void testEmptyCart() {
			
			//1. you need a cart
			//2. then you need to empty the cart
			//3. then you need to check how many items are in the cart Expected = 0
			//4.compare expected to actual
			//5. if expected == actual == PASS otherwise FAIL
			
			//1.
			ShoppingCart cart = new ShoppingCart();
			
			//2.
			cart.empty();
			
			//3.
			int numItems = cart.getItemCount();
			
			//4.
			assertEquals(0,numItems);
		
		}
		
		
		
		
		
		//test case 2 : adding items to the cart
		
			@Test
			public void testAddItem() {
				
				//1. create a shopping cart
				//2. make sure cart is empty
				//3. add a new item to cart
				//4. check how many items are in cart
				//5. compare actual and expected
				
				//1.
				ShoppingCart cart = new ShoppingCart();
				
				//2.
				cart.empty();
				
				//checking cart is empty first
				assertEquals(0, cart.getItemCount());
				
				//3.
				cart.addItem(new Product("Samsung",2000));
				
				//4.
				int numItems = cart.getItemCount();
				
				//5.
				assertEquals(1, numItems);
			
			}
			
}
